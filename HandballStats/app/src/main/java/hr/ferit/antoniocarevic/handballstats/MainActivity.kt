package hr.ferit.antoniocarevic.handballstats


import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.ExperimentalMaterial3Api

import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import hr.ferit.antoniocarevic.handballstats.ui.theme.HandballStatsTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        val viewModel by viewModels<HandballViewModel>()
        val matchViewModel by viewModels<MatchViewModel>()
        setContent {
            HandballStatsTheme {
                    NavigationController(viewModel, matchViewModel)
                }
            }

        }

    }





