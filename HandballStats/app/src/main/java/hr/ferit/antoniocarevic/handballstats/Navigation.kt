package hr.ferit.antoniocarevic.handballstats

import androidx.compose.material.icons.Icons
import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.R
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument

object Routes{
    const val SCREEN_BEGIN = "beginScreen"
    const val SCREEN_HOME = "homeScreen"
    const val SCREEN_TEAM = "teamScreen"
    const val SCREEN_ALL_MATCHES = "allMatchesScreen"
    const val SCREEN_MATCH = "matchScreen"
    const val SCREEN_PLAYER_DETAIL = "playerDetailScreen/{playerId}"
    const val SCREEN_ADD_PLAYER = "addPlayerScreen"
    const val SCREEN_MATCH_DETAIL = "matchDetailScreen/{matchId}"

    fun getPlayerDetailsPath(playerId: Int?) :String{
        if (playerId != null && playerId != -1){
            return "playerDetailScreen/$playerId"
        }
        return "playerDetailScreen/0"
    }

    fun getMatchDetailsPath(matchId: Int?) :String{
        if (matchId != null && matchId != -1){
            return "matchDetailScreen/$matchId"
        }
        return "matchDetailScreen/0"
    }

}

@Composable
fun NavigationController(
    handballViewModel: HandballViewModel,
    matchViewModel: MatchViewModel
) {
    val navController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = Routes.SCREEN_BEGIN
    ) {
        composable(route = Routes.SCREEN_HOME) {
            HomeScreen(navController = navController)
        }
        composable(route = Routes.SCREEN_BEGIN) {
            BeginScreen(navController = navController)
        }
        composable(route = Routes.SCREEN_TEAM) {
            TeamScreen(navController = navController, viewModel = handballViewModel)
        }
        composable(route = Routes.SCREEN_MATCH) {
            MatchScreen(navController = navController, viewModel = handballViewModel, matchViewModel = matchViewModel )
        }
        composable(route = Routes.SCREEN_ALL_MATCHES) {
            AllMatchesScreen(navController = navController, viewModel = matchViewModel)
        }
        composable(route = Routes.SCREEN_ADD_PLAYER){
            AddPlayerScreen(navController = navController, viewModel = handballViewModel)
        }
        composable(
            route = Routes.SCREEN_PLAYER_DETAIL,
            arguments = listOf(
                navArgument("playerId"){
                    type = NavType.IntType
                }
            )
        ) {backStackEntry ->
            backStackEntry.arguments?.getInt("playerId")?.let{
                PlayerDetailScreen(
                    navController = navController,
                    viewModel = handballViewModel,
                    playerId = it)
            }

        }
        composable(
            route = Routes.SCREEN_MATCH_DETAIL,
            arguments = listOf(
                navArgument("matchId"){
                    type = NavType.IntType
                }
            )
        ) {backStackEntry ->
            backStackEntry.arguments?.getInt("matchId")?.let{
                MatchDetailScreen(
                    navController = navController,
                    viewModel = matchViewModel,
                    matchId = it)
            }

        }
    }
}