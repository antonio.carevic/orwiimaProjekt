package hr.ferit.antoniocarevic.handballstats

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import hr.ferit.antoniocarevic.handballstats.ui.theme.MyColor
import hr.ferit.antoniocarevic.handballstats.ui.theme.fontFamily




@Composable
fun MatchScreen(
    navController: NavController,
    viewModel: HandballViewModel,
    matchViewModel: MatchViewModel
) {
    val participants = mutableListOf<Participant>()
    val scrollState = rememberLazyListState()

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = MyColor)
            .padding(5.dp),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ){
        TopBar(string = "Match",
            iconResource = R.drawable.ic_arrow_back,
            navController = navController)
        Row(
            modifier = Modifier
                .background(color = Color.White, shape = RoundedCornerShape(10.dp))
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceEvenly
        ) {
            Spacer(modifier = Modifier.width(66.dp))
            Column(
                modifier = Modifier.padding(vertical = 10.dp),

                ) {
                Text(text = "Ime")
            }

            Spacer(modifier = Modifier.width(97.dp))
            Column(
                modifier = Modifier.padding(10.dp)
            ) {
                Text(text = "Gol")
            }

            Spacer(modifier = Modifier.width(16.dp))
            Column(
                modifier = Modifier.padding(10.dp)
            ) {
                Text(text = "Assist")
            }

            Spacer(modifier = Modifier.width(6.dp))
            Column(
                modifier = Modifier.padding(10.dp)
            ) {
                Text(text = "2min")
            }

        }

        LazyColumn(
            state = scrollState,
            modifier = Modifier
                .fillMaxWidth()
                .height(625.dp)
                .background(color = MyColor),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally)
        {
            items(viewModel.playersData.size){

                var goals by remember { mutableStateOf(0) }
                var assists by remember { mutableStateOf(0) }
                var exclusions by remember { mutableStateOf(0) }


                val newParticipant = Participant(
                    name = viewModel.playersData[it].name,
                    goals = goals,
                    assists = assists,
                    exclusions = exclusions
                )
                participants.add(newParticipant)


                Spacer(modifier = Modifier.height(2.dp))

                Row(
                    modifier = Modifier
                        .background(color = Color.White, shape = RoundedCornerShape(10.dp))
                        .width(500.dp),
                    horizontalArrangement = Arrangement.SpaceAround
                ) {
                    Column(
                        modifier = Modifier.padding(10.dp)
                    ) {
                        Text(
                            text = viewModel.playersData[it].name,

                            )
                    }
                    Column(
                        modifier = Modifier.padding(10.dp)
                    ) {
                        Text(text = "$goals",
                            modifier = Modifier
                                .clickable {
                                    goals++
                                    viewModel.playersData[it].goals++
                                }
                        )
                    }
                    Column(
                        modifier = Modifier.padding(10.dp)
                    ) {
                        Text(text = "$assists",
                            modifier = Modifier
                                .clickable {
                                    assists++
                                    viewModel.playersData[it].assists++ }
                        )
                    }
                    Column(
                        modifier = Modifier.padding(10.dp)
                    ) {
                        Text(text = "$exclusions",
                            modifier = Modifier
                                .clickable {
                                    if(exclusions < 3) {
                                        exclusions++
                                        viewModel.playersData[it].exclusions++
                                    }

                                }
                        )
                    }

                }

            }


        }


        val match = Match(participants = participants, isFavorited = false)

        Spacer(modifier = Modifier.height(8.dp))
        UpdateButton(navController = navController, string = "Add match",viewModel = viewModel) {
            viewModel.updatePlayerData()
            matchViewModel.addNewMatch(match)
            navController.popBackStack()

        }



    }


}

@Composable
fun UpdateButton(
    navController: NavController,
    viewModel: HandballViewModel,
    string: String,
    onClick: () -> Unit
) {
    Box(
        modifier = Modifier
            .size(200.dp, 70.dp)
            .background(color = Color.White, shape = RoundedCornerShape(20.dp))
            .clickable {
                onClick()
            },
        contentAlignment = Alignment.Center
    ){
        Text(
            modifier = Modifier
                .padding(20.dp, 20.dp),
            text = string,
            fontSize = 18.sp,
            fontWeight = FontWeight.Medium,
            color = Color.Black
        )
    }
}