package hr.ferit.antoniocarevic.handballstats

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import hr.ferit.antoniocarevic.handballstats.ui.theme.MyColor

@Composable
fun MatchDetailScreen(
    navController: NavController,
    viewModel: MatchViewModel,
    matchId: Int
) {



    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = MyColor)
            .padding(5.dp),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ){
        TopBar(string = "Match",
            iconResource = R.drawable.ic_arrow_back,
            navController = navController)
        Row(
            modifier = Modifier
                .background(color = Color.White, shape = RoundedCornerShape(10.dp))
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceEvenly
        ) {
            Spacer(modifier = Modifier.width(66.dp))
            Column(
                modifier = Modifier.padding(vertical = 10.dp),

                ) {
                Text(text = "Ime")
            }

            Spacer(modifier = Modifier.width(97.dp))
            Column(
                modifier = Modifier.padding(10.dp)
            ) {
                Text(text = "Gol")
            }

            Spacer(modifier = Modifier.width(16.dp))
            Column(
                modifier = Modifier.padding(10.dp)
            ) {
                Text(text = "Assist")
            }

            Spacer(modifier = Modifier.width(6.dp))
            Column(
                modifier = Modifier.padding(10.dp)
            ) {
                Text(text = "2min")
            }
            
        }
        ParticipantList(match = viewModel.matchData[matchId])




    }


}

@Composable
fun ParticipantList(
    match:Match
) {
    val scrollState = rememberLazyListState()

    val participants = match.participants

    val finalParticipants = mutableListOf<Participant>()
    val names = mutableListOf<String>()

    participants.forEach{
        var person = Participant()

        for(participant in participants){
            if(it.name == participant.name){
                person = participant
            }
        }
        if(person.name !in names){
            names.add(person.name)
            finalParticipants.add(person)
        }


    }

    LazyColumn(
        state = scrollState,
        modifier = Modifier
            .fillMaxWidth()
            .height(625.dp)
            .background(color = MyColor),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally)
    {
        items(finalParticipants.size){

            Spacer(modifier = Modifier.height(2.dp))

            Row(
                modifier = Modifier
                    .background(color = Color.White, shape = RoundedCornerShape(10.dp))
                    .width(500.dp),
                horizontalArrangement = Arrangement.SpaceAround
            ) {
                Column(
                    modifier = Modifier.padding(10.dp)
                ) {
                    Text(
                        text = finalParticipants[it].name,

                        )
                }
                Column(
                    modifier = Modifier.padding(10.dp)
                ) {
                    Text(text = "${finalParticipants[it].goals}"
                    )
                }
                Column(
                    modifier = Modifier.padding(10.dp)
                ) {
                    Text(text = "${finalParticipants[it].assists}")
                }
                Column(
                    modifier = Modifier.padding(10.dp)
                ) {
                    Text(text = "${finalParticipants[it].exclusions}")
                }

            }

        }


    }
}