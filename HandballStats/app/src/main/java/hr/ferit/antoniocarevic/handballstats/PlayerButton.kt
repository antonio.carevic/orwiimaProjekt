package hr.ferit.antoniocarevic.handballstats

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController

@Composable
fun PlayerButton(
    name: String,
    onClick: () -> Unit
){
    Box(
        modifier = Modifier
            .size(225.dp, 70.dp)
            .padding(7.dp)
            .background(color = Color.White, shape = RoundedCornerShape(20.dp))
            .clickable {
                onClick()
            },
        contentAlignment = Alignment.Center,
    ){
       Row(
           verticalAlignment = Alignment.CenterVertically,
           horizontalArrangement = Arrangement.Center
       ){
           Icon(
               imageVector = Icons.Default.Person,
               contentDescription = null,
               modifier = Modifier
                   .size(50.dp, 40.dp)

           )
           Spacer(modifier = Modifier.width(16.dp))
           Text(
               text = name,
               modifier = Modifier
                   .weight(1f),
               fontSize = 18.sp,
               fontWeight = FontWeight.Medium,
               color = Color.Black,
           )
       }
    }
}



