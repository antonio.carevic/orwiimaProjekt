package hr.ferit.antoniocarevic.handballstats

import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.firestore

class MatchViewModel : ViewModel() {
    private val db = com.google.firebase.Firebase.firestore
    val matchData = mutableStateListOf<Match>()

    init {
        fetchDatabaseData()
    }

    private fun fetchDatabaseData() {
        db.collection("matches")
            .get()
            .addOnSuccessListener { result ->
                for (data in result.documents) {
                    val match = data.toObject(Match::class.java)
                    if (match != null) {
                        match.id = data.id
                        matchData.add(match)
                    }
                }
            }
    }

    fun updateMatch(match: Match) {
        db.collection("matches")
            .document(match.id)
            .set(match)
    }

    fun addNewMatch(match: Match) {
        db.collection("matches")
            .add(match)

    }
}