package hr.ferit.antoniocarevic.handballstats

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType.Companion.Text
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import hr.ferit.antoniocarevic.handballstats.ui.theme.MyColor
import hr.ferit.antoniocarevic.handballstats.ui.theme.WhiteClr
import hr.ferit.antoniocarevic.handballstats.ui.theme.fontFamily

@Composable
fun AddPlayerScreen(
    navController: NavController,
    viewModel: HandballViewModel
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = MyColor),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top
    ) {
        TopBar(
            string = "Player",
            iconResource = R.drawable.ic_arrow_back,
            navController = navController
        )
        Column(
            modifier = Modifier
                .width(400.dp)
                .height(500.dp)
                .padding(20.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Top
        ) {
            var name by remember { mutableStateOf("") }
            var position by remember { mutableStateOf("") }
            var age by remember { mutableStateOf("") }
            Text(
                modifier = Modifier
                    .padding(5.dp),
                text = "Name",
                fontSize = 16.sp,
                fontFamily = fontFamily,
                fontWeight = FontWeight.Bold,
                color = Color.White,
            )
            TextField(
                value = name,
                onValueChange = { newText ->
                    name = newText
                },
                modifier = Modifier
                    .background(color = MyColor),
                singleLine = true,
            )

            Spacer(modifier = Modifier.height(20.dp))

            Text(
                modifier = Modifier
                    .padding(5.dp),
                text = "Position",
                fontSize = 16.sp,
                fontFamily = fontFamily,
                fontWeight = FontWeight.Bold,
                color = Color.White,
            )
            TextField(
                value = position,
                onValueChange = { newText ->
                    position = newText
                },
                modifier = Modifier
                    .background(color = MyColor),
                singleLine = true,
            )

            Spacer(modifier = Modifier.height(20.dp))

            Text(
                modifier = Modifier
                    .padding(5.dp),
                text = "Age",
                fontSize = 16.sp,
                fontFamily = fontFamily,
                fontWeight = FontWeight.Bold,
                color = Color.White,
            )
            TextField(
                value = age,
                onValueChange = { newText ->
                    age = newText
                },
                modifier = Modifier
                    .background(color = MyColor),
                singleLine = true,
            )

            Spacer(modifier = Modifier.height(30.dp))
            UpdateButton(
                navController = navController,
                string = "Add player",
                viewModel = viewModel
            ) {
                viewModel.addNewPlayer(name, position, age)
                viewModel.updatePlayerData()
                navController.navigate(Routes.SCREEN_HOME)
            }
        }


    }
}
