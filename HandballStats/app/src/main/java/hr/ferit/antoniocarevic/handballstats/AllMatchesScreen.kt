package hr.ferit.antoniocarevic.handballstats

import androidx.annotation.DrawableRes
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ButtonElevation
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import hr.ferit.antoniocarevic.handballstats.ui.theme.MyColor
import hr.ferit.antoniocarevic.handballstats.ui.theme.fontFamily

@Composable
fun AllMatchesScreen(
    navController: NavController,
    viewModel: MatchViewModel) {

    var i = 1
    val scrollState = rememberLazyListState()
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = MyColor)
            .padding(4.dp),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        TopBar(string = "Matches", iconResource = R.drawable.ic_arrow_back, navController = navController)
        LazyColumn(
            state = scrollState,
            modifier = Modifier
                .fillMaxWidth()
                .height(600.dp)
                .background(color = MyColor),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally
        ){

            items(viewModel.matchData.size){
                MatchButton(
                    match = viewModel.matchData[it],
                    name = "Match $i",
                    viewModel = viewModel) {
                    navController.navigate(
                        Routes.getMatchDetailsPath(it)
                    )
                }
                Spacer(modifier = Modifier.height(5.dp))
                i++
            }
        }


    }
}


@Composable
fun MatchButton(
    match: Match,
    viewModel: MatchViewModel,
    name: String,
    onClick: () -> Unit
){
    Box(
        modifier = Modifier
            .size(225.dp, 70.dp)
            .padding(7.dp)
            .background(color = Color.White, shape = RoundedCornerShape(20.dp))
            .clickable {
                onClick()
            },
        contentAlignment = Alignment.Center,
    ){
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ){
            HelpButton(
                iconResource = R.drawable.ic_favorite,
                color = if(match.isFavorited) Color.Red else Color.Black
            ){
                match.isFavorited = !match.isFavorited
                viewModel.updateMatch(match)
            }
            Spacer(modifier = Modifier.width(16.dp))
            Text(
                text = name,
                modifier = Modifier
                    .weight(1f),
                fontSize = 18.sp,
                fontWeight = FontWeight.Medium,
                color = Color.Black,
            )
        }
    }
}

@Composable
fun HelpButton(
    @DrawableRes iconResource:Int,
    color : Color,
    onClick:() -> Unit = {}
) {
    Button(
        contentPadding = PaddingValues(),
        onClick = { onClick()},
        colors = ButtonDefaults.buttonColors(containerColor = Color.White, contentColor = color),
        modifier = Modifier
            .width(38.dp)
            .height(38.dp)
            .padding(horizontal = 5.dp)
    ) {
        Icon(
            painter = painterResource(id = iconResource),
            contentDescription = null
        )
    }
}

