package hr.ferit.antoniocarevic.handballstats


import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.firestore

class HandballViewModel: ViewModel() {
    private val db = com.google.firebase.Firebase.firestore

    val playersData = mutableStateListOf<Player>()

    init {
        fetchDatabaseData()
    }

    private fun fetchDatabaseData() {
        db.collection("players")
            .get()
            .addOnSuccessListener { result ->
                for (data in result.documents) {
                    val player = data.toObject(Player::class.java)
                    if (player != null) {
                        player.id = data.id
                        playersData.add(player)
                    }
                }
            }

    }

    fun updatePlayerData(){
        playersData.forEach{player ->
            db.collection("players")
                .document(player.id)
                .update(
                    mapOf(
                        "goals" to player.goals,
                        "assists" to player.assists,
                        "exclusions" to player.exclusions
                    )
                )
        }
    }

    fun addNewPlayer(
        name:String,
        position:String,
        age:String
    ){
        val newPlayer = hashMapOf(
            "name" to name,
            "position" to position,
            "age" to age,
            "goals" to 0,
            "assists" to 0,
            "exclusions" to 0
        )

        db.collection("players")
            .add(newPlayer)

    }

    fun removePlayer(player: Player){
        db.collection("players")
            .document(player.id)
            .delete()
    }

}

