document.addEventListener("DOMContentLoaded", function() {
    var darkModeEnabled = localStorage.getItem("darkModeEnabled");
    var element = document.body;
    if (darkModeEnabled === "true") {
        element.classList.add("darkMode");
    }
});

function switchColor() {
    var element = document.body;
    var darkModeEnabled = localStorage.getItem("darkModeEnabled");
    if (darkModeEnabled === "true") {
        element.classList.remove("darkMode");
        localStorage.setItem("darkModeEnabled", "false");
    } else {
        element.classList.add("darkMode");
        localStorage.setItem("darkModeEnabled", "true");
    }
}