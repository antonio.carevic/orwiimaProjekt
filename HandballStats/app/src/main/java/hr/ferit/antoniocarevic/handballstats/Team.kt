package hr.ferit.antoniocarevic.handballstats

data class Player(
    var id: String = "",
    val name: String = "",
    val age: String = "",
    val position: String = "",
    var goals: Int = 0,
    var assists: Int = 0,
    var exclusions: Int = 0,

    ){
    fun calculateScore(): Double{

        val score: Double

        if(exclusions == 0){
            score = ((goals + assists)/1).toDouble()
        }
        else {
            score = ((goals + assists) / exclusions).toDouble()
        }

        if (score <= 5){
            return score
        }
        else{
            return 5.0
        }
    }
}

data class Match(
    var id: String = "",
    val participants: List<Participant> = listOf(),
    var isFavorited: Boolean = false
)

data class Participant(
    val name: String = "",
    val goals: Int = 0,
    val assists: Int = 0,
    val exclusions: Int = 0,
)
