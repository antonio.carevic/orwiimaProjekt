package hr.ferit.antoniocarevic.handballstats

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import hr.ferit.antoniocarevic.handballstats.ui.theme.MyColor

@Composable
fun AppButon(
    text: String,
    navController: NavController,
    onClick: () -> Unit) {
    Box(
        modifier = Modifier
            .size(200.dp, 70.dp)
            .background(color = Color.White, shape = RoundedCornerShape(20.dp))
            .clickable{
                onClick()
            },
        contentAlignment = Alignment.Center
    ){
        Text(
            modifier = Modifier
                .padding(20.dp, 20.dp),
            text = text,
            fontSize = 18.sp,
            fontWeight = FontWeight.Medium,
            color = Color.Black
        )
    }
}
