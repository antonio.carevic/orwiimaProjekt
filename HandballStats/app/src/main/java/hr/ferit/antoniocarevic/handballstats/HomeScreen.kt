package hr.ferit.antoniocarevic.handballstats

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import hr.ferit.antoniocarevic.handballstats.ui.theme.MyColor
import hr.ferit.antoniocarevic.handballstats.ui.theme.fontFamily


@Composable
fun HomeScreen(navController: NavController) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = MyColor),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ){
        Text(
            modifier = Modifier
                .padding(top = 40.dp),
            text = "HandballStats",
            fontSize = 30.sp,
            fontFamily = fontFamily,
            fontWeight = FontWeight.Bold,
            color = Color.White,
        )


        Spacer(
            modifier = Modifier
                .padding(vertical = 100.dp)
        )
        AppButon(text = "Add match", navController = navController){
            navController.navigate(Routes.SCREEN_MATCH)
        }

        Spacer(
            modifier = Modifier
                .padding(vertical = 9.dp)
        )
        AppButon(text = "My team", navController = navController){
            navController.navigate(Routes.SCREEN_TEAM)
        }


        Spacer(
            modifier = Modifier
                .padding(vertical = 9.dp)
        )
        AppButon(text = "All matches", navController = navController){
            navController.navigate(Routes.SCREEN_ALL_MATCHES)
        }




    }

}



