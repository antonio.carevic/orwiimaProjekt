package hr.ferit.antoniocarevic.handballstats

import android.content.Intent.ShortcutIconResource
import androidx.annotation.DrawableRes
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import hr.ferit.antoniocarevic.handballstats.ui.theme.MyColor
import hr.ferit.antoniocarevic.handballstats.ui.theme.WhiteClr
import hr.ferit.antoniocarevic.handballstats.ui.theme.fontFamily

@Composable
fun TopBar(
    string: String,
    @DrawableRes iconResource: Int,
    navController: NavController) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .height(75.dp)
            .padding(horizontal = 8.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Absolute.Left
    ){
        BackButton(
            iconResource = iconResource,
            color = WhiteClr,
            ){
                navController.popBackStack()
        }
        Spacer(modifier = Modifier.width(120.dp))
        Text(
            text = string,
            fontSize = 28.sp,
            fontFamily = fontFamily,
            fontWeight = FontWeight.Bold,
            color = WhiteClr,
        )
    }
}

@Composable
fun BackButton(
    @DrawableRes iconResource: Int,
    color: Color,
    onClick:() -> Unit
    ) {
    Button(
        contentPadding = PaddingValues(),
        onClick = { onClick()},
        colors = ButtonDefaults.buttonColors(containerColor = MyColor, contentColor = color),
        shape= RoundedCornerShape(5.dp),
        modifier = Modifier
            .width(38.dp)
            .height(38.dp)
    ) {
        Icon(
            painter = painterResource(id = iconResource),
            contentDescription = null
        )
    }
}