package hr.ferit.antoniocarevic.handballstats

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import hr.ferit.antoniocarevic.handballstats.ui.theme.MyColor
import hr.ferit.antoniocarevic.handballstats.ui.theme.fontFamily

@Composable
fun PlayerDetailScreen(
    navController: NavController,
    viewModel: HandballViewModel,
    playerId:Int
) {
    val player = viewModel.playersData[playerId]
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = MyColor)
            .padding(end = 15.dp, start = 15.dp, bottom = 140.dp, top = 20.dp),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        TopBar(
            string = "Player",
            iconResource = R.drawable.ic_arrow_back,
            navController = navController
        )
        Spacer(modifier = Modifier.height(25.dp))
        Box(
            modifier = Modifier
                .background(color = Color.White, shape = RoundedCornerShape(20.dp))
                .fillMaxWidth()
                .height(500.dp)
                .padding(20.dp)
        ){
            Column {
                Row(
                    horizontalArrangement = Arrangement.Start,
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .fillMaxWidth()
                ){
                    Icon(
                        imageVector = Icons.Default.Person,
                        contentDescription = null,
                        modifier = Modifier
                            .size(100.dp)
                    )
                    Text(
                        text =player.name,
                        fontSize = 26.sp,
                        fontFamily = fontFamily,
                        fontWeight = FontWeight.Medium,
                        modifier = Modifier
                            .padding(start = 60.dp)
                    )
                }
                Spacer(modifier = Modifier.padding(25.dp))
                Row(
                    horizontalArrangement = Arrangement.Start,
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .fillMaxWidth()
                ){
                    Text(
                        text = "Position:",
                        fontSize = 22.sp,
                        fontFamily = fontFamily,
                        fontWeight = FontWeight.Normal,
                        modifier = Modifier
                            .padding(start = 10.dp, end = 2.dp, bottom = 5.dp)
                    )
                    Text(
                        text = player.position,
                        fontSize = 22.sp,
                        fontFamily = fontFamily,
                        fontWeight = FontWeight.Normal,
                        modifier = Modifier
                            .padding(start = 10.dp, end = 2.dp, bottom = 5.dp)
                    )
                }
                Row(
                    horizontalArrangement = Arrangement.Start,
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .fillMaxWidth()
                ){
                    Text(
                        text = "Age:",
                        fontSize = 22.sp,
                        fontFamily = fontFamily,
                        fontWeight = FontWeight.Normal,
                        modifier = Modifier
                            .padding(start = 10.dp, end = 2.dp, bottom = 5.dp)
                    )
                    Text(
                        text = player.age,
                        fontSize = 22.sp,
                        fontFamily = fontFamily,
                        fontWeight = FontWeight.Normal,
                        modifier = Modifier
                            .padding(start = 10.dp, end = 2.dp, bottom = 5.dp)
                    )
                }
                Spacer(modifier = Modifier.height(18.dp))
                Row(
                    horizontalArrangement = Arrangement.Start,
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .fillMaxWidth()
                ){
                    Text(
                        text = "Goals:",
                        fontSize = 22.sp,
                        fontFamily = fontFamily,
                        fontWeight = FontWeight.Normal,
                        modifier = Modifier
                            .padding(start = 10.dp, end = 2.dp, bottom = 5.dp)
                    )
                    Text(
                        text = "${player.goals}",
                        fontSize = 22.sp,
                        fontFamily = fontFamily,
                        fontWeight = FontWeight.Normal,
                        modifier = Modifier
                            .padding(start = 10.dp, end = 2.dp, bottom = 5.dp)
                    )
                }
                Row(
                    horizontalArrangement = Arrangement.Start,
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .fillMaxWidth()
                ){
                    Text(
                        text = "Assists:",
                        fontSize = 22.sp,
                        fontFamily = fontFamily,
                        fontWeight = FontWeight.Normal,
                        modifier = Modifier
                            .padding(start = 10.dp, end = 2.dp, bottom = 5.dp)
                    )
                    Text(
                        text = "${player.assists}",
                        fontSize = 22.sp,
                        fontFamily = fontFamily,
                        fontWeight = FontWeight.Normal,
                        modifier = Modifier
                            .padding(start = 10.dp, end = 2.dp, bottom = 5.dp)
                    )
                }
                Row(
                    horizontalArrangement = Arrangement.Start,
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .fillMaxWidth()
                ){
                    Text(
                        text = "Exclusion:",
                        fontSize = 22.sp,
                        fontFamily = fontFamily,
                        fontWeight = FontWeight.Normal,
                        modifier = Modifier
                            .padding(start = 10.dp, end = 2.dp, bottom = 5.dp)
                    )
                    Text(
                        text = "${player.exclusions}",
                        fontSize = 22.sp,
                        fontFamily = fontFamily,
                        fontWeight = FontWeight.Normal,
                        modifier = Modifier
                            .padding(start = 10.dp, end = 2.dp, bottom = 5.dp)
                    )
                }
                Row(
                    horizontalArrangement = Arrangement.Center,
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .fillMaxWidth()
                        .fillMaxHeight()
                ){
                    Spacer(modifier = Modifier.padding(10.dp))
                    Text(
                        text = "${player.calculateScore()} / 5.0",
                        fontSize = 44.sp
                    )

                }
            }
        }
        Spacer(modifier = Modifier.height(15.dp))
        UpdateButton(navController = navController, viewModel = viewModel, string ="Delete player" ) {
            viewModel.removePlayer(player)
            viewModel.updatePlayerData()
            navController.navigate(Routes.SCREEN_HOME)
        }
    }
}

